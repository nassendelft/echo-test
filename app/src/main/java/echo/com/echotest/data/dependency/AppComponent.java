package echo.com.echotest.data.dependency;

import javax.inject.Singleton;

import dagger.Component;
import echo.com.echotest.ui.test1.PrescriptionsActivity;
import echo.com.echotest.ui.test1.SelectMedicationActivity;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(PrescriptionsActivity activity);
    void inject(SelectMedicationActivity activity);
}
