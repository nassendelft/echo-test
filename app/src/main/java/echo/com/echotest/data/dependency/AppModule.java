package echo.com.echotest.data.dependency;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import echo.com.echotest.data.dao.DatabaseAccess;

@Module
public class AppModule {

    private final Application context;

    public AppModule(Application context) {
        this.context = context;
    }

    @Provides Context provideContext() {
        return context;
    }

    @Provides @Singleton DatabaseAccess provideDatabaseAccess() {
        return new DatabaseAccess();
    }
}
