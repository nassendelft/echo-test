package echo.com.echotest.data.domain.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Medication {

    private String category;
    private String detail;
    private String name;

    public Medication() {
    }

    public Medication(String category, String detail, String name) {
        this.category = category;
        this.detail = detail;
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public String getDetail() {
        return detail;
    }

    public String getName() {
        return name;
    }

    @Override public String toString() {
        return "Category: " + category + '\n' +
                "Detail: " + detail + '\n' +
                "Name: " + name;
    }
}
