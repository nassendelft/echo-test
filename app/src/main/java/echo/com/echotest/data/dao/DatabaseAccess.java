package echo.com.echotest.data.dao;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kelvinapps.rxfirebase.DataSnapshotMapper;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;

import java.util.List;

import echo.com.echotest.data.domain.model.Medication;
import echo.com.echotest.data.domain.model.User;
import rx.Observable;
import rx.subjects.BehaviorSubject;

public class DatabaseAccess {

    private final DatabaseReference database;
    private BehaviorSubject<User> userSubject = BehaviorSubject.create();
    private BehaviorSubject<List<Medication>> prescriptions = BehaviorSubject.create();

    public DatabaseAccess() {
        database = FirebaseDatabase.getInstance().getReference();

        RxFirebaseDatabase.observeValueEvent(database.child("user"), User.class)
                .subscribe(user -> userSubject.onNext(user));

        RxFirebaseDatabase.observeValueEvent(database.child("prescription"), DataSnapshotMapper.listOf(Medication.class))
                .subscribe(prescriptions -> this.prescriptions.onNext(prescriptions));
    }

    public Observable<User> getUser() {
        return userSubject.asObservable();
    }

    public Observable<List<Medication>> getPrescriptions() {
        return prescriptions.asObservable();
    }

    public Observable<List<Medication>> getMedications() {
        return RxFirebaseDatabase.observeSingleValueEvent(
                database.child("medication"),
                DataSnapshotMapper.listOf(Medication.class));
    }

    public void addPrescription(Medication medication) {
        database.child("prescription").push().setValue(medication);
    }

    public void updateUser(User user) {
        database.child("user").setValue(user);
    }

}
