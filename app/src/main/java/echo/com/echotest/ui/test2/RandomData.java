package echo.com.echotest.ui.test2;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigInteger;
import java.security.SecureRandom;

class RandomData implements Parcelable {

    private static final SecureRandom random = new SecureRandom();

    private final String title;

    public RandomData() {
        this.title = new BigInteger(130, random).toString(32);
    }

    String getTitle() {
        return title;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
    }

    protected RandomData(Parcel in) {
        this.title = in.readString();
    }

    public static final Parcelable.Creator<RandomData> CREATOR = new Parcelable.Creator<RandomData>() {
        @Override public RandomData createFromParcel(Parcel source) {
            return new RandomData(source);
        }

        @Override public RandomData[] newArray(int size) {
            return new RandomData[size];
        }
    };
}
