package echo.com.echotest.ui.test1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import javax.inject.Inject;

import echo.com.echotest.EchoApplication;
import echo.com.echotest.R;
import echo.com.echotest.data.dao.DatabaseAccess;
import echo.com.echotest.data.domain.model.Medication;

public class SelectMedicationActivity extends AppCompatActivity {

    @Inject DatabaseAccess databaseAccess;

    private ArrayAdapter<Medication> adapter;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, SelectMedicationActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_medication);
        EchoApplication.getAppComponent().inject(this);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        ListView listView = (ListView) findViewById(R.id.list_medication);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((adapterView, view, i, l) -> setSelected(i));

        databaseAccess.getMedications().subscribe(this::setMedications);
    }

    private void setMedications(List<Medication> medications) {
        adapter.addAll(medications);
        adapter.notifyDataSetChanged();
    }

    public void setSelected(int index) {
        databaseAccess.getMedications()
                .map(medications -> medications.get(index))
                .subscribe(this::addAndFinish);
    }

    private void addAndFinish(Medication medication) {
        databaseAccess.addPrescription(medication);
        finish();
    }
}
