package echo.com.echotest.ui.test1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.util.List;

import javax.inject.Inject;

import echo.com.echotest.EchoApplication;
import echo.com.echotest.R;
import echo.com.echotest.data.dao.DatabaseAccess;
import echo.com.echotest.data.domain.model.Medication;
import echo.com.echotest.data.domain.model.User;
import rx.subscriptions.CompositeSubscription;

public class PrescriptionsActivity extends AppCompatActivity {

    @Inject DatabaseAccess databaseAccess;

    private TextView txtName;
    private EditText inputName;
    private ViewSwitcher switcherName;
    private TextView txtEmail;
    private EditText inputEmail;
    private ViewSwitcher switcherEmail;
    private TextView txtBirthday;
    private EditText inputBirthday;
    private ViewSwitcher switcherBirthday;
    private TextView txtPrescriptions;

    private User user;
    private boolean inEditMode;

    private CompositeSubscription subscriptions = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EchoApplication.getAppComponent().inject(this);
        setContentView(R.layout.activity_prescriptions);

        txtName = (TextView) findViewById(R.id.txt_name);
        inputName = (EditText) findViewById(R.id.input_name);
        switcherName = (ViewSwitcher) findViewById(R.id.switcher_name);
        txtEmail = (TextView) findViewById(R.id.txt_email);
        inputEmail = (EditText) findViewById(R.id.input_email);
        switcherEmail = (ViewSwitcher) findViewById(R.id.switcher_email);
        txtBirthday = (TextView) findViewById(R.id.txt_birthdate);
        inputBirthday = (EditText) findViewById(R.id.input_birthdate);
        switcherBirthday = (ViewSwitcher) findViewById(R.id.switcher_birthdate);
        txtPrescriptions = (TextView) findViewById(R.id.txt_prescriptions);
        findViewById(R.id.img_add).setOnClickListener(view -> searchPrescriptions());
    }

    @Override protected void onStart() {
        super.onStart();
        subscriptions.add(databaseAccess.getUser().subscribe(this::setUser));
        subscriptions.add(databaseAccess.getPrescriptions().subscribe(this::setPrescriptions));
    }

    @Override protected void onStop() {
        super.onStop();
        subscriptions.unsubscribe();
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit, menu);
        return true;
    }

    @Override public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_edit).setIcon(inEditMode ? R.drawable.ic_done : R.drawable.ic_create);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
                enableEditMode(!inEditMode);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUser(User user) {
        this.user = user;

        txtName.setText(user.getName());
        txtEmail.setText(user.getEmail());
        txtBirthday.setText(user.getBirthdate());

        inputName.setText(user.getName());
        inputEmail.setText(user.getEmail());
        inputBirthday.setText(user.getBirthdate());
    }

    private void setPrescriptions(List<Medication> prescriptions) {
        String list = "";
        for (Medication prescription : prescriptions) {
            list += prescription.getName() + "\n\n";
        }
        txtPrescriptions.setText(list);
    }

    private void enableEditMode(boolean enable) {
        if (!enable) {
            user = new User(
                    inputName.getText().toString(),
                    user.getSurname(),
                    inputEmail.getText().toString(),
                    inputBirthday.getText().toString());
            databaseAccess.updateUser(user);
        }

        switcherName.showNext();
        switcherEmail.showNext();
        switcherBirthday.showNext();

        inEditMode = enable;
        invalidateOptionsMenu();
    }

    private void searchPrescriptions() {
        startActivity(SelectMedicationActivity.getStartIntent(this));
    }

}
