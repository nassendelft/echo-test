package echo.com.echotest.ui.test2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import echo.com.echotest.R;

class DataAdapter extends RecyclerView.Adapter<DataViewHolder> {

    private List<RandomData> items = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private DataViewHolder.OnItemClickListener onItemClickListener;

    DataAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
    }

    void setItems(List<RandomData> randomDatas) {
        items.clear();
        items.addAll(randomDatas);
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.item_random_data, parent, false);
        final DataViewHolder holder = new DataViewHolder(itemView);
        holder.itemView.setOnClickListener(view -> {
            int position = holder.getAdapterPosition();
            if (onItemClickListener != null && position != RecyclerView.NO_POSITION) {
                RandomData data = items.get(position);
                onItemClickListener.onItemClicked(data);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        RandomData data = items.get(position);
        holder.txtTitle.setText(data.getTitle());
    }

    @Override public int getItemCount() {
        return items.size();
    }

    void setOnItemClickListener(DataViewHolder.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
