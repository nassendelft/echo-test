package echo.com.echotest.ui.test2;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import echo.com.echotest.R;

class DataViewHolder extends RecyclerView.ViewHolder {

    final TextView txtTitle;

    DataViewHolder(View itemView) {
        super(itemView);
        txtTitle = (TextView) itemView.findViewById(R.id.txt_title);
    }

    interface OnItemClickListener {
        void onItemClicked(RandomData data);
    }
}
