package echo.com.echotest.ui.test2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.github.tbouron.shakedetector.library.ShakeDetector;

import echo.com.echotest.R;

public class RandomDataDetailsActivity extends AppCompatActivity {

    private static final String EXTRA_DATA = "extra_data";

    private static final int BACKGROUND_TRANSITION_DURATION = 800;

    private TransitionDrawable backgroundDrawable;

    public static Intent getStartIntent(Context context, RandomData data) {
        Intent intent = new Intent(context, RandomDataDetailsActivity.class);
        intent.putExtra(EXTRA_DATA, data);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_data_details);

        ShakeDetector.create(this, this::changeBackground);

        backgroundDrawable = createBackgroundDrawable();
        backgroundDrawable.setCrossFadeEnabled(true);

        findViewById(android.R.id.content).setBackground(backgroundDrawable);

        setData();
    }

    private TransitionDrawable createBackgroundDrawable() {
        return new TransitionDrawable(new Drawable[]{
                new ColorDrawable(getRandomColor()),
                new ColorDrawable(Color.WHITE)
        });
    }

    private int getRandomColor() {
        int r = (int) (Math.random() * 256);
        int g = (int) (Math.random() * 256);
        int b = (int) (Math.random() * 256);
        return Color.rgb(r, g, b);
    }

    private void changeBackground() {
        backgroundDrawable.startTransition(BACKGROUND_TRANSITION_DURATION);
    }

    @Override protected void onStart() {
        super.onStart();
        ShakeDetector.start();
    }

    @Override protected void onStop() {
        super.onStop();
        ShakeDetector.stop();
    }

    private void setData() {
        RandomData data = getIntent().getParcelableExtra(EXTRA_DATA);
        ((TextView) findViewById(R.id.txt_title)).setText(data.getTitle());
    }

}
