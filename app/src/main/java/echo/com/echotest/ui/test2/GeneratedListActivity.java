package echo.com.echotest.ui.test2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import echo.com.echotest.R;

public class GeneratedListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generated_list);

        final DataAdapter adapter = new DataAdapter(this);
        adapter.setItems(generateRandomData());
        adapter.setOnItemClickListener(this::openDetailsActivity);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list_generated);
        recyclerView.setAdapter(adapter);
    }

    private void openDetailsActivity(RandomData data) {
        startActivity(RandomDataDetailsActivity.getStartIntent(this, data));
    }

    private List<RandomData> generateRandomData() {
        final List<RandomData> items = new ArrayList<>();
        for (int i = 0; i < 500; i++) {
            items.add(new RandomData());
        }
        return items;
    }

}
