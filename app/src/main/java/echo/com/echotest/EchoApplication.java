package echo.com.echotest;

import android.app.Application;

import echo.com.echotest.data.dependency.AppComponent;
import echo.com.echotest.data.dependency.AppModule;
import echo.com.echotest.data.dependency.DaggerAppComponent;

public class EchoApplication extends Application {

    private AppComponent appComponent;

    private static EchoApplication INSTANCE;

    @Override public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public static AppComponent getAppComponent() {
        return INSTANCE.appComponent;
    }
}
