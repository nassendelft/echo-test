Just a few notes:

- There are 2 entries to this app found in your launcher. One for each
test.

- Build using RxJava and Firebase.

- RxJava is used to listen to any changes to the data and update the UI 
 accordingly.
 
- The database is set to public. For easy of this demo this makes sense.
Of course in an production environment this is not safe and should 
only be read/written based on proper authentication.

- A library for detecting shakes has been used (ShakeDetector). 

- I'm not that great in UI and there's not much to work off of I've
deliberately not put to much effort into it and focused more on 
 implementation.